#############################################################################
##
## Copyright (C) 2010 Riverbank Computing Limited.
## Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
## All rights reserved.
##
## This file is part of the examples of PyQt.
##
## $QT_BEGIN_LICENSE:BSD$
## You may use this file under the terms of the BSD license as follows:
##
## "Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are
## met:
##   * Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##   * Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in
##     the documentation and/or other materials provided with the
##     distribution.
##   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
##     the names of its contributors may be used to endorse or promote
##     products derived from this software without specific prior written
##     permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
## LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
## DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
## THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
## OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
## $QT_END_LICENSE$
##
#############################################################################


#############################################################################
##
## Copyright (C) 2013 Roman Kutlak, University of Aberdeen.
## All rights reserved.
##
## This file is part of SAsSy graph.
##
## You may use this file under the terms of the BSD license as follows:
##
## "Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are
## met:
##   * Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##   * Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in
##     the documentation and/or other materials provided with the
##     distribution.
##   * Neither the name of University of Aberdeen nor
##     the names of its contributors may be used to endorse or promote
##     products derived from this software without specific prior written
##     permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
## LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
## DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
## THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
## OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
##
#############################################################################


""" This package implements a graph drawing facility based on the Qt framework. 
This work is based on the ElasticNodes tutorial (Qt):
qt-project.org/doc/qt-4.7/graphicsview-elasticnodes.html

A python version of the code was obtained from:
github.com/Werkov/PyQt4/blob/master/examples/graphicsview/elasticnodes.py

The code was ported to PyQt5 and the functionality was extended.

There are three classes: Node, Edge and GraphWidget, which
serves as the main wieget that holds the graphics, scrollbars
and handles top level events.

The main purpose is drwing workflows or plans, but the GraphWidget should be 
versatile enough to allow drawing any tree or graph structure.

The nodes are rounded rectangles with optional text. Nodes that were moved
by the user retain their position and the spring-force layout can be disabled.

The widget also contains a simple layout algorithm, which lays out the graph
as a conventional workflow from left (start) to right (end).

"""


import math

from PyQt5 import QtCore, QtWidgets, QtGui, QtPrintSupport


class Graph:
    """ This class represents a graph (in the mathematical sense).
    A graph is a set of nodes and edges connecting the nodes. The nodes
    in the graph can be of any type, as long as they can be used keys in a dict.

    For more info about the graph structure see
    http://www.python.org/doc/essays/graphs.html
    
    Unlike normal graphs, this graph has a root node and an end node to support
    workflows better, as they usually have Start and End.

    """

    def __init__(self, items=None):
        self._items = dict()
        if items is not None:
            for k, v in items.items():
                self._items[k] = set(v)
        self.root = None
        self.top  = None

    def add_node(self, node, replace=False):
        """ Add a new node into the graph.
        If replace is True, replace the node if it exists (ie. delete 
        corresponding edges). Return true if added else return False.
        
        """
        if node not in self._items or replace:
            self._items[node] = set()
            return True
        return False

    def del_node(self, node):
        """ Remove the node from the graph, if it exists. """
        if node in self._items: del self._items[node]

    def add_edge(self, source, dest, create=True):
        """ Add a new edge to the graph.
        If create is true, create nodes if they are not in the graph.
        Return true if edge was added else return False.
        
        """
        if source not in self._items or dest not in self._items:
            if not create:
                return False
            else:
                self.add_node(source)
                self.add_node(dest)
        self._items[source].add(dest)
        return True

    def del_edge(self, source, dest):
        """ Remove the edge between source and dest, if it exists. """
        if source in self.nodes:
            self.nodes[source] = [x for x in self.nodes[source] if x != dest]

    def __str__(self):
        """ Return the summary of the graph. """
        num_edges = 0
        for n, t in self._items.items():
            num_edges += len(t)
        result = ('TaskGraph: #nodes: %d\t#edges %d' %
                    (len(self._items), num_edges))
        return result

    def __repr__(self):
        """ Return the string representation of the graph. """
        result = self.__str__() + '\n'
        for n, t in self._items.items():
            result += ('\t%s: %s\n' % (str(n), str(t)))
        return result

    @property
    def nodes(self):
        """ Returns nodes in the graph in arbitrary order. """
        return list(self._items.keys())

    @property
    def edges(self):
        """ Returns a list of pairs that represents the edges. """
        result = list()
        for n, t in self._items.items():
            for destination in t:
                result.append( (n, destination) )
        return result

    def __contains__(self, item):
        """ Return true if a node (item) is in the graph. """
        return (item in self._items)

    def __getitem__(self, k):
        """ Return the list of nodes connected to the node k or raise exception
        if the node is not in the graph.

        """
        return self._items[k]

    def items(self):
        return [ x for x in self._items.items() ]

    def depth_first_nodes(self, start, visited=None):
        """ Return nodes from start using depth first algorithm. Note that 
        if there is no edge to a particular node, it will not be visited.
        
        """
        yield start
        if visited is None: visited = set()
        visited.add(start)
        for t in self[start]:
            if t in visited: continue
            yield from self.depth_first_nodes(t, visited)

    def depth_first_nodes_with_level(self, start, visited=None, lvl=0):
        """ Return nodes from start using depth first algorithm. Note that 
        if there is no edge to a particular node, it will not be visited.
        
        """
        yield (start, lvl)
        if visited is None: visited = set()
        visited.add(start)
        for t in self[start]:
            if t in visited: continue
            yield from self.depth_first_nodes_with_level(t, visited, lvl+1)

    def breadth_first_nodes(self, start):
        """ Return nodes from start using breadth first algorithm. Note that
        if there is no edge to a particular node, it will not be visited.
        
        """
        visited = set()
        queue = [start]
        while len(queue) > 0:
            t = queue.pop() # take the last element
            if t not in visited:
                visited.add(t)
                yield t
                if len(self[t]) > 0:
                    queue = list(self[t]) + queue # prepend the new nodes

    def breadth_first_nodes_with_level(self, start):
        """ Return nodes from start using breadth first algorithm. Note that
        if there is no edge to a particular node, it will not be visited.
        
        """
        visited = set()
        lvl = 0
        queue = [ (start, lvl) ]
        while len(queue) > 0:
            t, current = queue.pop() # take the last element
            if t not in visited:
                visited.add(t)
                yield (t, current)
                if len(self[t]) > 0:
                    queue = [(x, current+1) for x in self[t]] + queue # prepend the new nodes

    def find_path(self, start, end, path=[]):
        """ Find path between start and end. If there is no such path, return 
        None. If start is end return [start].
        
        """
        path = path + [start]
        if start == end:
            return path
        if not start in self:
            return None
        for node in self[start]:
            if node not in path:
                newpath = self.find_path(node, end, path)
                if newpath: return newpath
        return None

    def find_all_paths(self, start, end, path=[]):
        """ Find all paths between start and end in a list.
            If there are no paths, return an empty list.
        
        """
        path = path + [start]
        if start == end:
            return [path]
        if not start in self:
            return []
        paths = []
        for node in self[start]:
            if node not in path:
                newpaths = self.find_all_paths(node, end, path)
                for newpath in newpaths:
                    paths.append(newpath)
        return paths

    def find_shortest_path(self, start, end, path=[]):
        """ Find the shortest path between start and end.
            If there is no such path, return None.
            If start is end return [start].

        """
        path = path + [start]
        if start == end:
            return path
        if not start in self:
            return None
        shortest = None
        for node in self[start]:
            if node not in path:
                newpath = self.find_shortest_path(node, end, path)
                if newpath:
                    if not shortest or len(newpath) < len(shortest):
                        shortest = newpath
        return shortest


class Edge(QtWidgets.QGraphicsItem):
    """ This class represents the edges between individual nodes.
    The class stores references to the source and destination nodes and uses
    these for calculating its coordinates in the QGraphicsScene.

    """
    Pi = math.pi
    TwoPi = 2.0 * Pi

    Type = QtWidgets.QGraphicsItem.UserType + 1

    def __init__(self, sourceNode, destNode):
        super(Edge, self).__init__()

        self.hidden = False
        self.arrowSize = 20.0
        self.sourcePoint = QtCore.QPointF()
        self.destPoint = QtCore.QPointF()

        self.setAcceptedMouseButtons(QtCore.Qt.NoButton)
        self.source = sourceNode
        self.dest = destNode
        self.source.addEdge(self)
        self.dest.addEdge(self)
        self.adjust()
        self.setZValue(1)

    def type(self):
        return Edge.Type

    def sourceNode(self):
        return self.source

    def setSourceNode(self, node):
        self.source = node
        self.adjust()

    def destNode(self):
        return self.dest

    def setDestNode(self, node):
        self.dest = node
        self.adjust()

    def adjust(self):
        if not self.source or not self.dest: return

        line = QtCore.QLineF(self.mapFromItem(self.source, 0, 0),
                             self.mapFromItem(self.dest, 0, 0))
        length = line.length()

        self.prepareGeometryChange()

        # if the graph is in freeArrows state, allow arrows pointing to any part
        # of the node
        if not self.source.graphWidget.freeArrows:
#            self.sourcePoint = line.p1()

            src = self.source
            self.sourcePoint = self.mapFromItem(src,
                                                src.boundingRect().right(),
                                                src.rectangle.center().y())
            self.destPoint = line.p2()
            if length > 20.0:
                edgeOffset = QtCore.QPointF((line.dx() * 5) / length,
                                            (line.dy() * 5) / length)

#                # move the edge of the arrow from under the node
#                contains = self.source.boundingRect().contains # shorthand method call
#                while (contains(self.mapToItem(self.source, self.sourcePoint))):
#                    self.sourcePoint += edgeOffset

                contains = self.dest.shape().contains # ditto
                while (contains(self.mapToItem(self.dest, self.destPoint))):
                    self.destPoint -= edgeOffset

                # only point to the sid
                tmp = self.mapFromItem(self.dest, self.dest.boundingRect().topLeft())
                self.destPoint.setX(tmp.x())

        else:
            src = self.source
            self.sourcePoint = line.p1()
            self.destPoint = line.p2()

            if length > 20.0:
                edgeOffset = QtCore.QPointF((line.dx() * 5) / length,
                                            (line.dy() * 5) / length)

#                # move the edge of the arrow from under the node
                contains = self.source.shape().contains # shorthand method call
                while (contains(self.mapToItem(self.source, self.sourcePoint))):
                    self.sourcePoint += edgeOffset

                contains = self.dest.shape().contains # ditto
                while (contains(self.mapToItem(self.dest, self.destPoint))):
                    self.destPoint -= edgeOffset

    def boundingRect(self):
        if not self.source or not self.dest: return QtCore.QRectF()
        penWidth = 1.0
        extra = (penWidth + self.arrowSize) / 2.0
        h = self.destPoint.x() - self.sourcePoint.x()
        w = self.destPoint.y() - self.sourcePoint.y()
        return QtCore.QRectF(self.sourcePoint, QtCore.QSizeF(h, w))\
                .normalized().adjusted(-extra, -extra, extra, extra)

    def paint(self, painter, option, widget):
        # do not draw edges for hidden nodes
        if self.hidden or self.source.hidden or self.dest.hidden: return
        # NOTE the trick of drawing stuff with BG colour does not work...
        colour = QtCore.Qt.black

        if not self.source or not self.dest: return
        # Draw the line itself.
        line = QtCore.QLineF(self.sourcePoint, self.destPoint)

        if line.length() == 0.0: return

        painter.setPen(QtGui.QPen(colour, 1, QtCore.Qt.SolidLine,
                QtCore.Qt.RoundCap, QtCore.Qt.RoundJoin))
        painter.drawLine(line)

        # Draw the arrows if there's enough room.
        angle = math.acos(line.dx() / line.length())
        if line.dy() >= 0: angle = Edge.TwoPi - angle

        arr = self.arrowSize
        destArrowP1 = self.destPoint + \
            QtCore.QPointF(math.sin(angle - Edge.Pi / 3) * arr,
                           math.cos(angle - Edge.Pi / 3) * arr)
        destArrowP2 = self.destPoint + \
            QtCore.QPointF(math.sin(angle - Edge.Pi + Edge.Pi / 3) * arr,
                           math.cos(angle - Edge.Pi + Edge.Pi / 3) * arr)

        painter.setBrush(colour)
        painter.drawPolygon(
            QtGui.QPolygonF([line.p2(), destArrowP1, destArrowP2]))


class DecoratorPlacementError(Exception):
    pass


class DecoratorShapeError(Exception):
    pass


class Decorator(QtWidgets.QGraphicsItem):
    """ Class Decorator is used to graphically embellish nodes. For example,
    in case of drawing a YAWL workflow, nodes might have markings 
    for splits and joins.

    """
    Type = QtWidgets.QGraphicsItem.UserType + 3

    def __init__(self, parent, side):
        super(Decorator, self).__init__(parent=parent)
        self.setFlag(QtWidgets.QGraphicsItem.ItemSendsGeometryChanges)
        self.setCacheMode(QtWidgets.QGraphicsItem.DeviceCoordinateCache)
        self.setZValue(2)
        self.parent = parent
        self.symbol = None
        self.w = 0
        self.h = 0
        bounds = self.parent.boundingRect()
        border = self.parent.border/2
        if side == 'left':
            self.w = min(0.3*self.parent.w, 30)
            self.h = self.parent.h
            self.setPos(bounds.left() - self.w/2 + border, 0)
        elif side == 'right':
            self.w = min(0.3*self.parent.w, 30)
            self.h = self.parent.h
            self.setPos(bounds.right() + self.w/2 - border, 0)
        elif side == 'top':
            self.h = min(0.4*self.parent.h, 40)
            self.w = self.parent.w
            self.setPos(0, bounds.top() - self.h/2 + border)
        elif side == 'bottom':
            self.h = min(0.4*self.parent.h, 40)
            self.w = self.parent.w
            self.setPos(0, bounds.bottom() + self.h/2 - border)
        self.x = -self.w/2
        self.y = -self.h/2

    def boundingRect(self):
        border = self.parent.border
        rectangle = QtCore.QRectF(
                QtCore.QPointF(self.x - border/2, self.y - border/2),
                QtCore.QSizeF(self.w + border, self.h + border))
        return rectangle

    def paint(self, painter, option, widget):
        """ Paint the shape of the object.
        Use a gradient for the brush and set pen width to the border width.

        """
        if self.parent.hidden: return
        painter.setBrush(self.parent.colour)
        painter.setPen(QtGui.QPen(self.parent.borderColour,
                                  self.parent.border,
                                  QtCore.Qt.SolidLine,
                                  QtCore.Qt.RoundCap,
                                  QtCore.Qt.RoundJoin))

        rectangle = QtCore.QRectF(QtCore.QPointF(self.x, self.y),
                                  QtCore.QSizeF(self.w, self.h))
        painter.drawRect(rectangle)

        if self.symbol:
#            painter.setBrush(QtGui.QBrush(QtGui.QColor('black')))
            painter.setBrush(QtCore.Qt.NoBrush)
            painter.setPen(QtGui.QPen(QtGui.QColor('black'),
                            self.parent.border,
                            QtCore.Qt.SolidLine,
                            QtCore.Qt.RoundCap,
                            QtCore.Qt.RoundJoin))
            painter.drawPolygon(self.symbol)

    def mousePressEvent(self, event):
        self.parent.mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        self.parent.mouseReleaseEvent(event)

    def mouseMoveEvent(self, event):
        self.parent.mouseMoveEvent(event)

    def _getSides(self):
        border = self.parent.border
        l = -(self.w)/2 +  border
        r =  (self.w)/2 -  border
        t = -(self.h)/2 +  border
        b =  (self.h)/2 -  border
        return (l,t,r,b)


class LeftTriangleDecorator(Decorator):
    Type = QtWidgets.QGraphicsItem.UserType + 4

    def __init__(self, parent, side='left'):
        super(LeftTriangleDecorator, self).__init__(parent, side)
        self.decorator_type = 'left'

    def paint(self, painter, option, widget):
        """ Recalculate the shape. """
        offset = self.parent.border/2
        l, t, r, b = self._getSides()
        pointsCoord = [[ r+offset, t ],
                       [ l, 0 ],
                       [ r+offset, b ]]
        self.symbol = QtGui.QPolygonF()
        for i in pointsCoord:
                self.symbol.append(QtCore.QPointF(i[0], i[1]))
        super(LeftTriangleDecorator, self).paint(painter, option, widget)


class RightTriangleDecorator(Decorator):
    Type = QtWidgets.QGraphicsItem.UserType + 4

    def __init__(self, parent, side='right'):
        super(RightTriangleDecorator, self).__init__(parent, side)
        self.decorator_type = 'right'

    def paint(self, painter, option, widget):
        """ Recalculate the shape. """
        offset = self.parent.border/2
        l, t, r, b = self._getSides()
        pointsCoord = [[ l-offset, t ],
                       [ r, 0 ],
                       [ l-offset, b ]]
        self.symbol = QtGui.QPolygonF()
        for i in pointsCoord:
                self.symbol.append(QtCore.QPointF(i[0], i[1]))
        super(RightTriangleDecorator, self).paint(painter, option, widget)


class UpTriangleDecorator(Decorator):
    Type = QtWidgets.QGraphicsItem.UserType + 4

    def __init__(self, parent, side='top'):
        super(UpTriangleDecorator, self).__init__(parent, side)
        self.decorator_type = 'up'

    def paint(self, painter, option, widget):
        """ Recalculate the shape. """
        offset = self.parent.border/2
        l, t, r, b = self._getSides()
        pointsCoord = [[ l, b+offset ],
                       [ r, b+offset ],
                       [ 0, t ]]
        self.symbol = QtGui.QPolygonF()
        for i in pointsCoord:
                self.symbol.append(QtCore.QPointF(i[0], i[1]))
        super(UpTriangleDecorator, self).paint(painter, option, widget)


class DownTriangleDecorator(Decorator):
    Type = QtWidgets.QGraphicsItem.UserType + 4

    def __init__(self, parent, side='bottom'):
        super(DownTriangleDecorator, self).__init__(parent, side)
        self.decorator_type = 'down'

    def paint(self, painter, option, widget):
        """ Recalculate the shape. """
        offset = self.parent.border/2
        l, t, r, b = self._getSides()
        pointsCoord = [[ l, t-offset ],
                       [ 0, b ],
                       [ r, t-offset ]]
        self.symbol = QtGui.QPolygonF()
        for i in pointsCoord:
                self.symbol.append(QtCore.QPointF(i[0], i[1]))
        super(DownTriangleDecorator, self).paint(painter, option, widget)


class DiamondDecorator(Decorator):
    Type = QtWidgets.QGraphicsItem.UserType + 4

    def __init__(self, parent, side='left'):
        super(DiamondDecorator, self).__init__(parent, side)
        self.decorator_type = 'diamond'

    def paint(self, painter, option, widget):
        """ Recalculate the shape. """
        l, t, r, b = self._getSides()
        pointsCoord = [[0,t],
                       [l,0],
                       [0,b],
                       [r,0]]
        self.symbol = QtGui.QPolygonF()
        for i in pointsCoord:
                self.symbol.append(QtCore.QPointF(i[0], i[1]))
        super(DiamondDecorator, self).paint(painter, option, widget)


class Node(QtWidgets.QGraphicsItem):
    """ Class Node represents a node in a graph.
    The Node has the shape of a rounded rectangle, which can have text inside.
    
    The nodes are dragable and dragging a node around the screen anchors it
    in the position where it was dropped. Other nodes retain the spring-force
    layout, unless it was disabled on all nodes.

    """
    Type = QtWidgets.QGraphicsItem.UserType + 2

    def __init__(self, graphWidget, data, label_fn=str, max_width=-1):
        super(Node, self).__init__()
        self.label_fn = label_fn
        self.graphWidget = graphWidget
        self.edgeList = []
        self.newPos = QtCore.QPointF()

        self.setFlag(QtWidgets.QGraphicsItem.ItemIsMovable)
        self.setFlag(QtWidgets.QGraphicsItem.ItemSendsGeometryChanges)
        self.setCacheMode(QtWidgets.QGraphicsItem.DeviceCoordinateCache)
        self.setZValue(3)

        self.data = data
        self.text = self.label_fn(data)
        self.max_width = max_width
        self.diameter = 10
        self.padding = 5
        self.border  = 2
#        self.colour = QtGui.QColor('#BDBDBD')
        self.font_size = 14
        self.colour = QtGui.QColor('gray').lighter(200)
        self.borderColour = QtGui.QColor('black')
        self.labelItem = None
        self.create_label(self.text)
        self.leftDecorator = None
        self.rightDerocator = None
        self.topDecorator = None
        self.bottomDecorator = None
        self.forceLayout = False
        self.hidden = False
        self.rounded = True
        self.setShape('rounded_rectangle')

    def __str__(self):
        return '%f, %f, %f, %f' % (self.x, self.y, self.w, self.h)

    def __repr__(self):
        local = str(self)
        tl = self.mapToScene(self.x, self.y)
        scene = '%f, %f, %f, %f' % (tl.x(), tl.y(), tl.x() + self.w, tl.y() + self.h)
        return (local + ' --> ' + scene)

    def create_label(self, text):
        """ Create a label item and set the text. 
        If there already is a label item in the node, remove it.

        """
        if self.labelItem is not None:
            self.labelItem.setParentItem(None)
        # preserve old values
        italic = (self.labelItem and self.labelItem.font().italic()) or False
        bold = (self.labelItem and self.labelItem.font().bold()) or False
        # create new labelItem
        labelItem = QtWidgets.QGraphicsTextItem(text, parent=self)
#        labelItem.setTextInteractionFlags(QtCore.Qt.TextSelectableByMouse)
        font = QtGui.QFont()
        font.setPointSize(self.font_size)
        font.setItalic(italic)
        font.setBold(bold)
        labelItem.setFont(font)
        tw = labelItem.document().size().width()
        th = labelItem.document().size().height()
        if (tw < 2*th): tw = 2*th
        if self.max_width != -1:
            tw = min(tw, self.max_width)
        labelItem.setTextWidth(tw)
        th = labelItem.document().size().height()
        labelItem.setPos(-tw/2, -th/2)

        # center the text using a cursor - really a hack...
        format = QtGui.QTextBlockFormat();
        format.setAlignment(QtCore.Qt.AlignCenter);
        cursor = labelItem.textCursor();
        cursor.select(QtGui.QTextCursor.Document);
        cursor.mergeBlockFormat(format);
        cursor.clearSelection();
        labelItem.setTextCursor(cursor);
        self.labelItem = labelItem
        # now re-calculate the minimal space required for the node (label)
        self._calculate_points()

    def _calculate_points(self):
        """ Calculate the coordinates of the minimum size based on the text. """
        tw = self.labelItem.document().size().width()
        th = self.labelItem.document().size().height()

        self.w = self.padding + tw + self.padding
        self.h = self.padding + th + self.padding
        self.x = -self.w/2
        self.y = -self.h/2
        self.rectangle = QtCore.QRectF(QtCore.QPointF(self.x, self.y),
                                       QtCore.QSizeF(self.w, self.h))
        self.borderRectangle = QtCore.QRectF(
                QtCore.QPointF(self.x - self.border/2, self.y - self.border/2),
                QtCore.QSizeF(self.w + self.border, self.h + self.border))
                
    def type(self):
        return Node.Type

    def addEdge(self, edge):
        self.edgeList.append(edge)
        edge.adjust()

    def edges(self):
        return self.edgeList

    def setShape(self, shape):
        if 'default' == str(shape):
            self.nodeShape = RoundedRectangleShape(self)
        elif 'rounded_rectangle' == str(shape):
            self.nodeShape = RoundedRectangleShape(self)
        elif 'rectangle' == str(shape):
            self.nodeShape = RectangleShape(self)
        elif isinstance(Shape, shape):
            self.nodeShape = shape
        else:
            raise ShapeError('Unknown shape: "%s"' % str(shape))

    def update(self):
        if self.leftDecorator: self.leftDecorator.update()
        if self.rightDerocator: self.rightDerocator.update()
        if self.topDecorator: self.topDecorator.update()
        if self.bottomDecorator: self.bottomDecorator.update()
        super().update()

    def setColour(self, colour):
        self.colour = QtGui.QColor(colour)
        self.update()
        
    def setBorderColour(self, colour):
        self.borderColour = QtGui.QColor(colour)
        self.update()

    def setBorderSize(self, size):
        self.border = size
        self.update()

    def setItalic(self, val):
        font = self.labelItem.font()
        font.setItalic(val)
        self.labelItem.setFont(font)
        self.update()

    def setBold(self, val):
        font = self.labelItem.font()
        font.setBold(val)
        self.labelItem.setFont(font)
        self.update()

    def setFontSize(self, size):
        self.prepareGeometryChange()
        self.create_label(self.text, size)
        self.update()

    def setHidden(self, flag):
        """ Disable or enable drawing the node. """
        self.hidden = flag
        if flag:
            self.labelItem.hide()
        else:
            self.labelItem.show()
        for e in self.edgeList:
            e.hidden = flag
        for edge in self.edgeList:
            edge.adjust()
        self.update()

    def calculateForces(self):
        if (not self.forceLayout or
            not self.scene() or
            self.scene().mouseGrabberItem() is self):
            self.newPos = self.pos()
            return

        sceneRect = self.scene().sceneRect()

        # if the task graph has a root, it starts on the left
        if (self.graphWidget.graph.root in self.graphWidget.nodes and
                self.graphWidget.nodes[self.graphWidget.graph.root] is self):
            x = sceneRect.left() + self.w/2 + self.graphWidget.horizontal_gap
            y = sceneRect.center().y() - self.h + self.graphWidget.offset
            self.newPos = QtCore.QPointF(x,y)
            return

        # if the task graph has a top, it ends on the right
        if (self.graphWidget.graph.root in self.graphWidget.nodes and
                self.graphWidget.nodes[self.graphWidget.graph.top] is self):
            x = sceneRect.right() - self.w/2 - self.graphWidget.horizontal_gap
            y = sceneRect.center().y() - self.h + self.graphWidget.offset
            self.newPos = QtCore.QPointF(x, y)
            return

        # Sum up all forces pushing this item away.
        xvel = 0.0
        yvel = 0.0
        for item in self.scene().items():
            if not isinstance(item, Node):
                continue

            line = QtCore.QLineF(self.mapFromItem(item, 0, 0),
                    QtCore.QPointF(0, 0))
            dx = line.dx()
            dy = line.dy()
            l = (dx * dx + dy * dy) / 3
            if l > 0:
                xvel += (dx * 120) / l
                yvel += (dy * 150) / l

        # push the nodes from the edges
        # ...somehow
        center = self.graphWidget.scene().sceneRect().center()
        center.setY(center.y() + self.graphWidget.offset / 2)
        this = self.mapToScene(QtCore.QPointF(0, 0))
        line = QtCore.QLineF(this, center)
#        print_point(center)
#        print_point(this)
        dx = line.dx()
        dy = line.dy()
#        print('dy: %d' % (dy))
        l = (dx * dx + dy * dy)
        if l > 0:
            diff = (dy * 50) / l
#            print('diff: %f' % (diff))
            yvel += diff

        # Now subtract all forces pulling items together.
        weight = (len(self.edgeList) + 1) * 8.0
        for edge in self.edgeList:
            if edge.sourceNode() is self:
                pos = self.mapFromItem(edge.destNode(), 0, 0)
            else:
                pos = self.mapFromItem(edge.sourceNode(), 0, 0)
            xvel += pos.x() / weight
            yvel += pos.y() / weight

        if QtCore.qAbs(xvel) < 0.3 and QtCore.qAbs(yvel) < 0.3:
            xvel = yvel = 0.0

        self.newPos = self.pos() + QtCore.QPointF(xvel, yvel)
        # push the node inside the scene
        width = self.w/2 + self.border/2
        height = self.h/2 + self.border/2
        self.newPos.setX(min(max(self.newPos.x(),
                                 sceneRect.left() + width),
                             sceneRect.right() - width))
        self.newPos.setY(min(max(self.newPos.y(),
                         sceneRect.top() + height + self.graphWidget.offset),
                         sceneRect.bottom() - height))

    def advance(self):
        if self.newPos == self.pos(): return False
        self.setPos(self.newPos)
        return True

    def boundingRect(self):
        """ Define the bounds of the node.
        The bounds depend on the shape, which should be large enough to contain
        the necessary text. 
        
        """
        rect = self.nodeShape.boundingRect()
        offset = 1.5*self.border
        x = self.borderRectangle.left()
        y = self.borderRectangle.top()
        w = self.borderRectangle.width()
        h = self.borderRectangle.height()
        if self.leftDecorator:
            x = x - self.leftDecorator.boundingRect().width() + offset
            w = w + self.leftDecorator.boundingRect().width() - offset
        if self.rightDerocator:
            w = w + self.rightDerocator.boundingRect().width() - offset
        if self.topDecorator:
            y = y - self.topDecorator.boundingRect().height() + offset
            h = h + self.topDecorator.boundingRect().height() - offset
        if self.bottomDecorator:
            h = h + self.bottomDecorator.boundingRect().height() - offset

        rect = QtCore.QRectF(QtCore.QPointF(x, y),
                             QtCore.QSizeF(w, h))
        return rect

    def shape(self):
        """ The shape method defines the area that reacts to mouse clicks. 
        If it was not defined, the area returned by boundingRect() whould
        define the clickable area. Use the current node shape.

        """
        return self.nodeShape.shape()

    def paint(self, painter, option, widget):
        """ Paint the shape of the object and the label.
        The shape can be set as nodeShape attribute. If self.hidden is true,
        do not paint the node.
        """
        if self.hidden: return
        self.nodeShape.paint(painter, option, widget)
        # text is a child of this item so it will be painted automatically

    def _createGradient(self, option):
        gradient = QtGui.QRadialGradient(0, 0, self.w)
        gradient.setCenter(0, 0)
        if option.state & QtWidgets.QStyle.State_Sunken: # when clicked on
            gradient.setColorAt(1, self.colour.darker(150))
            gradient.setColorAt(0, self.colour.lighter(150))
        else:
            gradient.setColorAt(0, self.colour)
            gradient.setColorAt(1, self.colour.darker(150))
        return gradient

    def addDecorator(self, shape, side):
        decorator = None
        dim = 20
        if 'left_triangle' == shape:
            decorator = LeftTriangleDecorator(self, side)
        elif 'right_triangle' == shape:
            decorator = RightTriangleDecorator(self, side)
        elif 'up_triangle' == shape:
            decorator = UpTriangleDecorator(self, side)
        elif 'down_triangle' == shape:
            decorator = DownTriangleDecorator(self, side)
        elif 'diamond' == shape:
            decorator = DiamondDecorator(self, side)
        else:
            raise ('Unnown decorator shape: "' + str(shape) + '"')
        if decorator is not None:
            if 'left' == side: self.leftDecorator = decorator
            elif 'right' == side: self.rightDerocator = decorator
            elif 'top' == side: self.topDecorator = decorator
            elif 'bottom' == side: self.bottomDecorator = decorator
            else:
                raise DecoratorPlacementError('"%s" is not a valid side'
                                              % str(side))
        self.nodeShape = RectangleShape(self)

    def removeDecorator(self, side):
        if 'left' == side:
            self.leftDecorator.setParentItem(None)
            self.leftDecorator = None
        elif 'right' == side:
            self.rightDerocator.setParentItem(None)
            self.rightDerocator = None
        elif 'top' == side:
            self.topDecorator.setParentItem(None)
            self.topDecorator = None
        elif 'bottom' == side:
            self.bottomDecorator.setParentItem(None)
            self.bottomDecorator = None
        else:
            raise DecoratorPlacementError('"%s" is not a valid side'
                                          % str(side))

    def itemChange(self, change, value):
        if change == QtWidgets.QGraphicsItem.ItemPositionHasChanged:
            for edge in self.edgeList:
                edge.adjust()
            self.graphWidget.itemMoved()
        return super(Node, self).itemChange(change, value)

    def mousePressEvent(self, event):
        self.update()
        tl = self.mapToScene(self.x, self.y)
        print('(%d, %d) : %s' % (tl.x(), tl.y(), repr(self.data)))
        super(Node, self).mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        self.update()
        super(Node, self).mouseReleaseEvent(event)

    def mouseMoveEvent(self, event):
        self.forceLayout = False
        super(Node, self).mouseMoveEvent(event)


class GraphWidget(QtWidgets.QGraphicsView):
    """ This class represents a widget that can hold the graph and draw it.
    The class serves as a grphical representation of the graph, all 
    manipulations (e.g., adding nodes or edges) should be done 
    on the underlying Graph instance, followed by call to updateGraphics().

    The GraphWidget allows two kinds of layout: manual and spring-force based.
    The spring-force layout can be enabled by call to setSpringForce(True).

    """
    def __init__(self, graph, description="", label_fn=str):
        super(GraphWidget, self).__init__()
        scene = QtWidgets.QGraphicsScene(self)
        scene.setItemIndexMethod(QtWidgets.QGraphicsScene.NoIndex)
        scene.setSceneRect(0, 0, self.width(), self.height())
        self.setScene(scene)
        self.setCacheMode(QtWidgets.QGraphicsView.CacheBackground)
        self.setViewportUpdateMode(QtWidgets.QGraphicsView.BoundingRectViewportUpdate)
        self.setRenderHint(QtGui.QPainter.Antialiasing)
        self.setRenderHint(QtGui.QPainter.TextAntialiasing)
        self.setTransformationAnchor(QtWidgets.QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QtWidgets.QGraphicsView.AnchorViewCenter)

        self.setDescription(description)

        self.scale(1.0, 1.0)
        self.currentFactor = 1.0

#        self.setMinimumSize(200, 200)
        self.lastX = (self.width() - 20) / 2
        self.lastY = (self.height() - 20) / 2
        self.horizontal_gap = 60
        self.vertical_gap = 50
        # space around descr
        self.offset = (self.description.boundingRect().height() + 20)
        # calculated by autoLayout
        self.preferred_width = 0
        self.preferred_height = 0

        self.label_fn = label_fn
        self.timerId = 0
        self.freeArrows = False
        self.forceLayout = False
        self.graph = graph
        self.nodes = dict()
        self.edges = dict()
        self._construct_graph()
        self.hidden_nodes = set()

    def setDescription(self, description):
        scene = self.scene()
        labelItem = QtWidgets.QGraphicsTextItem()
        labelItem.setTextWidth(scene.width() - 20)
        labelItem.setPos(10, 10)
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        labelItem.setFont(font)
#        labelItem.setPlainText(description)
        # format the text to the center of the textitem
        format = QtGui.QTextBlockFormat();
        format.setAlignment(QtCore.Qt.AlignCenter);
        cursor = labelItem.textCursor();
        cursor.select(QtGui.QTextCursor.Document);
        cursor.mergeBlockFormat(format);
        cursor.clearSelection();
        labelItem.setTextCursor(cursor);

        self.description = labelItem
        scene.addItem(labelItem)

    def __str__(self):
        return str(self.graph)

    def __repr__(self):
        return ('GraphWidget: ' + str(self))

#    def updateGraphics(self):
#        """ Re-draw the graph. This is the public interface method, that calls
#        the private implementation method. 
#        
#        """
#        self._reconstruct_graph()
#
#    def _reconstruct_graph(self):
#        """ Remove all items from the scene and construct a new scene with
#        the graph.
#        
#        """
#        scene = self.scene()
#        for k, v in self.nodes.items():
#            scene.removeItem(v)
#        for k, v in self.edges.items():
#            scene.removeItem(v)
#        self.nodes = dict() # mapping from graph to scene items
#        self.edges = dict() # ditto
#        self._construct_graph()

    def _construct_graph(self):
        """ Construct the graphical representation from the underlying graph."""
        if self.graph is None: return

        max_width = self.getUniformNodeWidth(self.graph.nodes, 14)
        for n in self.graph.nodes:
            node = Node(self, n, self.label_fn, max_width)
            self._addNode(n, node)
        for e in self.graph.edges:
            self.createEdge(e, False)
        self.setDefaultColours()

    def setDefaultColours(self):
        """ Set the node colours to default. """
        for node in self.items():
            if isinstance(node, Node):
                if (hasattr(self.graph, 'root') and
                    node is not None and
                    self.graph.root == node.data):
                    node.setColour('#80FF00')
                elif (hasattr(self.graph, 'top') and
                      node is not None and
                      self.graph.top == node.data):
                    node.setColour('#F79F81')
                elif node is not None:
                    node.setColour(QtGui.QColor('gray').lighter(180))

    def getUniformNodeWidth(self, items, font_size):
        """ Calculate average text width and return recommended maximum. """
        # check what is the longest word
        max_word = ''
        max_word_len = 0
        
        total = 0
        count = 0
        for node in items:
            font = QtGui.QFont()
            font.setPointSize(font_size)
            text = self.label_fn(node)
            for w in text.split():
                if len(w) > max_word_len:
                    max_word_len = len(w)
                    max_word = w
            labelItem = QtWidgets.QGraphicsTextItem(text)
            labelItem.setFont(font)
            tw = labelItem.document().size().width()
            th = labelItem.document().size().height()
            if (tw < 2*th): tw = 2*th
            total += tw
            count += 1

        if count == 0:
            return -1
        mean = total / count

        total_diff = 0
        for node in items:
            labelItem = QtWidgets.QGraphicsTextItem(self.label_fn(node))
            labelItem.setFont(font)
            tw = labelItem.document().size().width()
            th = labelItem.document().size().height()
            if (tw < 2*th): tw = 2*th
            x = tw - mean
            total_diff += x * x
        sd = math.sqrt(total_diff / count)
        recommended_width = (mean*0.5 + 0.5*sd)
        
        # check that we can fit in the longest word without breaking it:
        labelItem = QtWidgets.QGraphicsTextItem(max_word)
        labelItem.setFont(font)
        tw = labelItem.document().size().width()
        th = labelItem.document().size().height()
        if (tw < 2*th): tw = 2*th
        print("Longest word: {0}".format(max_word))
        # return the bigger one
        return max(recommended_width, tw)

    def createNode(self, name, max_width=-1):
        """ Add a node to the graphical representation. """
        self.graph.add_node(name)
        n = Node(self, name, self.label_fn, max_width)
        self.nodes[name] = n
        self.scene().addItem(n)
        n.setPos(self.width()/2 - n.w/2, self.height()/2 - n.h/2)
        return n

    def _addNode(self, name, node, pos=None):
        self.nodes[name] = node
        self.scene().addItem(node)
        if pos is None: pos = (self.width()/2 - node.w/2,
                               self.height()/2 - node.h/2)
        node.setPos(*pos)
        
    def removeNode(self, node):
        """ Remove a node from the graph widget. """
        if node not in self.nodes: return None
        # find the edges to this node
        item = self.nodes[node]
        self.scene().removeItem(item)
        del self.nodes[node]
        

    def createEdge(self, edge, create=True):
        """ Add an edge to the graphical representation. By default, nodes
        are created if they don't exist. If you don't want to add any new nodes,
        pass the parameter create with the value False.
        
        """
        source, dest = edge
        if source not in self.nodes or dest not in self.nodes:
            if not create:
                return None
            else:
                self.createNode(source)
                self.createNode(dest)

        self.graph.add_edge(source, dest, create)
        
        src = self.nodes[source]
        dst = self.nodes[dest]
        e = Edge(src, dst)
        self.edges[edge] = e
        self.scene().addItem(e)
        return e

    def getStart(self):
        if hasattr(self.graph, 'root'):
            return self.graph.root
        else:
            return None

    def getFinish(self):
        if hasattr(self.graph, 'top'):
            return self.graph.top
        else:
            return None

    def getNode(self, name):
        return self.nodes[name]

    def setNode(self, name, node):
        if name not in self.nodes:
            self._addNode(name, node)
        else:
            self.nodes[name] = node

    def hideNode(self, node):
        self.hidden_nodes.add(node)
        self.nodes[node].setHidden(True)
        self.update()

    def showNode(self, node):
        if node in self.hidden_nodes: self.hidden_nodes.remove(node)
        self.nodes[node].setHidden(False)

    def setColour(self, node, colour):
        """ Set the colour of the node, if it is in the graph. """
        if node in self.nodes: self.nodes[node].setColour(colour)
        else: print('%s not in graph' % repr(node))

    def setBorderColour(self, node, colour):
        """ Set the colour of the border of node. """
        if node in self.nodes: self.nodes[node].setBorderColour(colour)
        else: print('%s not in graph' % repr(node))

    def getGraph(self):
        """ Return the underlying graph. """
        return self.graph

    def forceLayout(self, val):
        """ Turn the spring-force layout on(True)/off(False)."""
        self.forceLayout = val
        nodes = [n for n in self.scene().items() if isinstance(n, Node)]
        for n in nodes:
            n.forceLayout = val
        self.itemMoved()
        
    def setFreeArrows(self, flag):
        """ Set the internal flag that allows the arrows to point to any part
        of a node. If flag is False, arrows point only to the left/right edge
        of each node.
        
        """
        self.freeArrows = flag

    def autoLayout(self, groups=None):
        """ Reposition the nodes in the scene as a workflow from left to right,
        trying to avoid crossing edges. The nodes are ordered from left to
        right, starting from root, going to end. If no root or end are set, 
        an arbitrary node is set as the root. NOTE: It is assumed that 
        the graph is fully connected so that the nodes can be reached through
        bredth first search.

        """
        levels = realign_graph(self.graph)
#        print(levels)
        self.calculatePreferredSize(levels)

        if groups is not None:
            print('Groups:')
            for x in groups.items():
                print(x)
            levels = self.orderLevelsByGroups(levels, groups)

#        print(levels)

        hg = self.horizontal_gap
        vg = self.vertical_gap

        x = hg
        for lvl in range(0, len(levels)):
            nodes = list(map(lambda x: self.nodes[x], levels[lvl]))
            w = max([n.boundingRect().width() for n in nodes])
            h = sum([n.boundingRect().height() for n in nodes])
            h += (len(nodes)-1) * vg
            x += w/2
            y = ((self.preferred_height / 2) - (h/2) + self.offset)
            for n in nodes:
                nh = n.boundingRect().height()
                n.setPos(x, y - nh/2)
                y += nh + vg
            x += w/2 + hg
        self.update()

    def calculatePreferredSize(self, levels):
        """ Calculate the preferred size of the canvas given a graph split into 
        levels. The levels are based on the distanc from the root node.
        
        """
        hg = self.horizontal_gap
        vg = self.vertical_gap

        max_h = 0
        total_h = 0
        total_w = 0
        for lvl in range(0, len(levels)):
            nodes = list(map(lambda x: self.nodes[x], levels[lvl]))
            w = max([n.boundingRect().width() for n in nodes])
            h = sum([n.boundingRect().height() for n in nodes])
            h += (len(nodes)-1) * vg
            total_w += w + hg
            max_h = max(max_h, h)

        total_h = max_h + vg + self.offset + vg
        total_w += hg

        self.preferred_width = total_w
        self.preferred_height = total_h

    def orderLevelsByGroups(self, levels, groups):
        """ Order nodes on each level to correspond to the order specified 
        by groups. That is any item in the current level that belongs 
        to the first group should be before any item that belongs 
        to the second group, etc.
        
        """
        new_levels = dict()
        for lvl in range(0, len(levels)):
            items = set(levels[lvl])
            print('\n\n' + str(items))
            # partition items by groups
            current_level_groups = dict()
            used = set()
            for group in groups.keys():
                items_in_group = set(groups[group]) & items
                used |= items_in_group
                current_level_groups[group] = items_in_group
            rest = items - used
            print(rest)
            current_level_groups[len(groups)] = rest
            new_levels[lvl] = []
            for i in range(len(current_level_groups)):
                new_levels[lvl].extend(current_level_groups[i])
        print('__')
        return new_levels

    def itemMoved(self):
        if not self.timerId:
            self.timerId = self.startTimer(1000 / 50)

    def keyPressEvent(self, event):
        key = event.key()

        if key == QtCore.Qt.Key_Plus:
            self.scaleView(1.3)
        elif key == QtCore.Qt.Key_Minus:
            self.scaleView(1 / 1.3)
        else:
            super(GraphWidget, self).keyPressEvent(event)

    def resizeEvent(self, event):
        max_width = self.width()-10
        max_height = self.height()-10

        if max_width > self.preferred_width:
            new_w = max_width
        else:
            new_w = self.preferred_width

        if max_height > self.preferred_height:
            new_h = max_height
        else:
            new_h = self.preferred_height

        scene = self.scene().sceneRect()
        self.scene().setSceneRect(0, 0, new_w, new_h)
        self.description.setTextWidth(new_w - 20)
        self.description.setPos(10, 10)
        self.itemMoved()
        super(GraphWidget, self).resizeEvent(event)

    def timerEvent(self, event):
        nodes = [item for item in self.scene().items()
                    if isinstance(item, Node)]
        for node in nodes:
            node.calculateForces()
        itemsMoved = False
        for node in nodes:
            if node.advance():
                itemsMoved = True
        if not itemsMoved:
            self.killTimer(self.timerId)
            self.timerId = 0

    def drawBackground(self, painter, rect):
        sceneRect = self.sceneRect()
#        painter.setBrush(QtGui.QColor('#EEE'))
        painter.setBrush(QtGui.QColor('white'))
        painter.drawRect(sceneRect)

    def scaleView(self, scaleFactor):
        if scaleFactor == 1.0:
            scaleFactor = 1/self.currentFactor
        factor = self.transform().scale(scaleFactor, scaleFactor)\
        .mapRect(QtCore.QRectF(0, 0, 1, 1)).width()
        if factor < 0.07 or factor > 100: return
        self.scale(scaleFactor, scaleFactor)
        self.currentFactor *= scaleFactor

    def fitToWidith(self, width):
        """ Scale the view to fit given width. """
        factor = width/(self.scene().sceneRect().width() * self.currentFactor)
        self.scaleView(factor)

    def savePDF(self, filename='/tmp/graph.pdf'):
        printer = QtPrintSupport.QPrinter(QtPrintSupport.QPrinter.HighResolution)
        printer.setOrientation(QtPrintSupport.QPrinter.Landscape);
#        printer.setPageSize(QtPrintSupport.QPrinter.A4)
        printer.setOutputFormat(QtPrintSupport.QPrinter.PdfFormat)
        printer.setOutputFileName(filename)

        painter = QtGui.QPainter(printer)
        painter.setRenderHint(QtGui.QPainter.Antialiasing)
        painter.setRenderHint(QtGui.QPainter.TextAntialiasing)
        self.scene().render(painter);
        painter.end()


def print_point(point):
    """ Utility function for printing points. """
    print('(%f, %f)' % (point.x(), point.y()))


def realign_graph(graph):
    """ Split the graph to levels - distance from the root node.
        Nodes that would point backwards are moved forward to try to minimise
        crossing edges and nodes.

        """
    levels = dict()
    bfn = graph.breadth_first_nodes_with_level
    start = graph.root
    if start is None:
        if len(graph.nodes) > 0:
            start = graph.nodes[0]
        else:
            return levels

    for n, lvl in bfn(start):
        if lvl not in levels:
            levels[lvl] = []
        levels[lvl].append(n)

    # the nodes can point backwards, fix it
    messy = True
    while messy:
        messy = False
        for i in range(0, len(levels)):
            for source in levels[i]:
                destinations = graph[source]
                for d in destinations:
                    for j in range(0, i+1):
                        if d in levels[j]:
                            # d has level lower or equal to source
                            # - move it forward
                            # unless there is a loop
                            if graph.find_path(d, source) is None:
                                levels[j].remove(d)
                                if (i + 1) not in levels:
                                    levels[i + 1] = []
                                levels[i+1].append(d)
                                messy |= True
                        else:
                            messy |= False
    return levels


################################### Shapes #####################################


class ShapeError(Exception):
    pass


class Shape(QtWidgets.QGraphicsItem):
    """ Abstract class for Node shapes. 
    Subclasses should implement methods shape(), boundingRect() and paint().
    The size of the shape should be calculated inside these methods because
    changing the size of the font of a node affects the minimal size of the 
    node. As such, Node shapes should not store any co-ordinates but 
    calculate them as needed.

    """
    Type = QtWidgets.QGraphicsItem.UserType + 3

    def __init__(self, parent):
        super(Shape, self).__init__()
        self.setFlag(QtWidgets.QGraphicsItem.ItemIsMovable)
        self.setFlag(QtWidgets.QGraphicsItem.ItemSendsGeometryChanges)
        self.setCacheMode(QtWidgets.QGraphicsItem.DeviceCoordinateCache)
        self.setZValue(2)
        self.parent = parent

    # FIXME: this probably does not work as intended
    #   - when I use it in Decorator it does not forward clicks
    def __getattr__(self, name):
        """ Forward all calls to the parent. """
        def method(*args):
            print("tried to handle unknown method " + name)
            if args:
                print("it had arguments: " + str(args))

        if self.parent.hasattr(name):
            m = self.parent.getattr(name)
            m(*args)


class RectangleShape(Shape):
    """ Rectangular shape for nodes. """

    def __init__(self, parent):
        """ Initialise the Shape class. Pass the parent to Shape. """
        super(RectangleShape, self).__init__(parent)

    def boundingRect(self):
        """ The area belonging to the shape. This should include the border. """
        b = self.parent.border
        p = self.parent
        rectangle = QtCore.QRectF(QtCore.QPointF(p.x - b/2, p.y - b/2),
                                  QtCore.QSizeF(p.w + b, p.h + b))
        return rectangle

    def shape(self):
        """ The shape method defines the area that reacts to mouse clicks. """
        path = QtGui.QPainterPath()
        path.addRect(self.boundingRect())
        return path

    def paint(self, painter, option, widget):
        """ Paint the shape of the object. """
        painter.setBrush(QtGui.QBrush(self.parent._createGradient(option)))
        painter.setPen(QtGui.QPen(self.parent.borderColour,
                                  self.parent.border,
                                  QtCore.Qt.SolidLine,
                                  QtCore.Qt.RoundCap,
                                  QtCore.Qt.RoundJoin))
        p = self.parent
        rectangle = QtCore.QRectF(QtCore.QPointF(p.x, p.y),
                                  QtCore.QSizeF(p.w, p.h))
        painter.drawRect(rectangle)


class RoundedRectangleShape(Shape):
    """ Rectangular shape for nodes with rounded corners. """

    def __init__(self, parent, diameter=10):
        """ Initialise the Shape class. Pass the parent to Shape. """
        super(RoundedRectangleShape, self).__init__(parent)
        self.diameter = diameter

    def boundingRect(self):
        """ The area belonging to the shape. This should include the border. """
        b = self.parent.border
        p = self.parent
        rectangle = QtCore.QRectF(QtCore.QPointF(p.x - b/2, p.y - b/2),
                                  QtCore.QSizeF(p.w + b, p.h + b))
        return rectangle

    def shape(self):
        """ The shape method defines the area that reacts to mouse clicks. """
        path = QtGui.QPainterPath()
        path.addRoundedRect(self.boundingRect(), self.diameter, self.diameter)
        return path

    def paint(self, painter, option, widget):
        """ Paint the shape of the object. """
        painter.setBrush(QtGui.QBrush(self.parent._createGradient(option)))
        painter.setPen(QtGui.QPen(self.parent.borderColour,
                                  self.parent.border,
                                  QtCore.Qt.SolidLine,
                                  QtCore.Qt.RoundCap,
                                  QtCore.Qt.RoundJoin))
        p = self.parent
        rectangle = QtCore.QRectF(QtCore.QPointF(p.x, p.y),
                                  QtCore.QSizeF(p.w, p.h))
        painter.drawRoundedRect(rectangle,
                                self.diameter, self.diameter)


class ShapeTester(QtWidgets.QGraphicsItem):
    """ Class that serves as a scratchpad for shape-drawing code.

    """
    Type = QtWidgets.QGraphicsItem.UserType + 11

    def __init__(self, parent):
        super(ShapeTester, self).__init__(parent=parent)
        self.setFlag(QtWidgets.QGraphicsItem.ItemIsMovable)
        self.setFlag(QtWidgets.QGraphicsItem.ItemSendsGeometryChanges)
        self.setCacheMode(QtWidgets.QGraphicsItem.DeviceCoordinateCache)
        self.setZValue(11)
        self.parent = parent
        self.hidden = False
        self.border = 2
        self.x = 100
        self.y = 100
        self.w = 200
        self.h = 200
        self.colour = QtGui.QColor('#369')
        self.borderColour = QtGui.QColor('black')

    def boundingRect(self):
        border = self.border
        rectangle = QtCore.QRectF(
                QtCore.QPointF(self.x - border/2, self.y - border/2),
                QtCore.QSizeF(self.w + border, self.h + border))
        return rectangle

    def shape(self):
        """ The shape method defines the area that reacts to mouse clicks. 
        If it was not defined, the area returned by boundingRect() whould
        define the clickable area.

        """
        border = self.border
        rectangle = QtCore.QRectF(
                QtCore.QPointF(self.x - border/2, self.y - border/2),
                QtCore.QSizeF(self.w + border, self.h + border))
        path = QtGui.QPainterPath()
        path.addRect(rectangle)
        return path

    def paint(self, painter, option, widget):
        """ Paint the shape of the object.
        Use a gradient for the brush and set pen width to the border width.

        """
        if self.hidden: return
#        painter.setBrush(QtCore.Qt.NoBrush)
        painter.setBrush(QtGui.QBrush(self.colour))
        painter.setPen(QtGui.QPen(self.borderColour,
                                  self.border,
                                  QtCore.Qt.SolidLine,
                                  QtCore.Qt.RoundCap,
                                  QtCore.Qt.RoundJoin))

        rectangle = QtCore.QRectF(QtCore.QPointF(self.x, self.y),
                                  QtCore.QSizeF(self.w, self.h))
#        painter.drawRect(rectangle)
# http://stackoverflow.com/questions/15288708/drawing-rectangle-with-only-2-corners-rounded-in-qt
        path = QtGui.QPainterPath();
        path.setFillRule( QtCore.Qt.WindingFill );
        path.addRoundedRect( rectangle, 20, 20 );
        path.addRect( self.w, self.x, self.y, self.x ); # Top right corner not rounded
        path.addRect(  self.x, self.h, self.y, self.x ); # Bottom left corner not rounded
        painter.drawPath( path.simplified() ); # Only Top left & bottom right corner rounded

    def mousePressEvent(self, event):
        self.update()
        super(ShapeTester, self).mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        self.update()
        super(ShapeTester, self).mouseReleaseEvent(event)

    def mouseMoveEvent(self, event):
        super(ShapeTester, self).mouseMoveEvent(event)



