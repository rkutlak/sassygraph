#############################################################################
##
## Copyright (C) 2013 Roman Kutlak, University of Aberdeen.
## All rights reserved.
##
## This file is part of SAsSy graph.
##
## You may use this file under the terms of the BSD license as follows:
##
## "Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are
## met:
##   * Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##   * Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in
##     the documentation and/or other materials provided with the
##     distribution.
##   * Neither the name of University of Aberdeen nor
##     the names of its contributors may be used to endorse or promote
##     products derived from this software without specific prior written
##     permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
## LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
## DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
## THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
## OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
##
#############################################################################

# NOTE: This package is deprecated - its functionality was replaced by qtgraph.

import pygraphviz as gv
import re

import xml.sax as sax
from xml.dom import minidom

import formats.yawl as yawl


def default_node_colour_fn(id, workflow):
    node = None
    if id in workflow.task_graph:
        node = workflow.task(id)
    if node is None: return 'blue'
    if node.is_input_condition: return 'green'
    if node.is_output_condition: return 'red'
    if node.is_condition: return 'orange'
    return 'blue'


def create_graph_from(workflow, path,
                      node_colour_fn=default_node_colour_fn,
                      edge_colour_fn=None):
    """Create a graph of the workflow.
    
    workflow (dstruct.Workflow): workflow to graph
    node_colour_fn a callable object that can determine a node colour
    edge_colour_fn a callable object that can determine an edge colour
    
    The colour functions will be called 'c = node_colour_fn(node, workflow)'
    and should return a string that graphviz can use as a colour.

    """
    if not workflow:
        return
    
    G = gv.AGraph(strict=False, directed=True, rankdir="LR")
    steps_label = repr(workflow.task_graph)
    steps_label = steps_label.replace('TaskGraph: ', '')
    G.graph_attr['label']="%s" % (workflow.description)
    G.node_attr['shape']='oval'

    for n in workflow.task_graph.nodes:
        colour = ('grey' if node_colour_fn is None
            else node_colour_fn(n, workflow))
        G.add_node(n, color=colour)

    for e in workflow.task_graph.edges:
        colour = ('black' if edge_colour_fn is None
            else edge_colour_fn(e, workflow))
        G.add_edge(e, color=colour)

    G.draw(path, prog='dot')


def create_graph(plan):
    """Create a graph of the plan
    
    plan (dstruct.Plan): plan to graph
    
    """
    if not plan:
        return

    G = gv.AGraph(strict=False, directed=True)#, rankdir="LR"
    G.graph_attr['label']="%s (%s steps)" % (plan.name, plan.length)
    G.node_attr['shape']='circle'
    G.edge_attr['color']='blue'

    G.add_node("Start", color='red')
    
    last_node = "Start"
    i = 1
    
    for action_id in plan.sequence:
        G.add_edge(last_node, i, label=plan.actions[action_id].name)
        last_node = i
        i += 1

    G.add_node("Goal", color='green')
    G.add_edge(last_node, "Goal")

    G.write("/tmp/plan.dot")
    G.draw("/tmp/plan.png", prog='dot')


def create_graph_yawl(file):
    G = gv.AGraph(strict=False, directed=True)#, rankdir="LR"
    G.node_attr['shape']='circle'
    G.edge_attr['color']='blue'

    G.add_node("Start", color='red')

    data = yawl.parse_yawl(file)
    G.graph_attr['label']="%s (%s steps)" % (data.description, len(data.tasks))
    
    # get the task ids and assign them to numbers
    nodes = dict()
    i = 1
    
    # the inputCondition marks the start of the flow
    first = data.input_condition.flows_into[0]
    nodes[first] = "Start"
    
    goals = data.output_conditions
    for g in goals:
        nodes[g] = "Goal"
        idx = g.find("_")
        if idx > 0:
            nodes[g] += g[idx:]
        G.add_node(nodes[g], color='green')

    for k in data.tasks.keys(): # k is id (string)
        if k == first:
            pass
        elif k in goals:
            pass
        else:
            nodes[k] = i
            G.add_node(i)
            i += 1

    # now go through the tasks again and add edges
    for k, v in data.tasks.items(): # k is id (string) v is Task
        for flow in v.flows_into:
            #print "\tflow is: " + flow
            G.add_edge(nodes[k], nodes[flow], label=v.name.strip())

    G.write("/tmp/plan.dot")
    G.draw("/tmp/plan.png", prog='dot')


def create_graph_orderings(graph_data):
    length, graph = graph_data
    G = gv.AGraph(strict=False, directed=True)#, rankdir="LR"
    G.node_attr['shape'] = 'circle'
    G.edge_attr['color'] = 'blue'

    G.add_node('Start', color='orange')
    G.add_node('Goal', color='green')
    # collect all values as these ar not starting nodes
    dependenies = list()
    for k, values in graph.items():
        for v in values:
            G.add_edge(k, v)
            dependenies.append(v)

    # actions that are not in the dependency graph link to the end
    # actions that are not in dependencies are links to the start
    for i in range(length):
        if i not in graph.keys():
            G.add_edge(i, "Goal")
        if i in graph.keys() and i not in dependenies:
            G.add_edge("Start", i)

    G.write("/tmp/orderings.dot")
    G.draw("/tmp/orderings.png", prog="dot")






