#############################################################################
##
## Copyright (C) 2013 Roman Kutlak, University of Aberdeen.
## All rights reserved.
##
## This file is part of SAsSy graph.
##
## You may use this file under the terms of the BSD license as follows:
##
## "Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are
## met:
##   * Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##   * Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in
##     the documentation and/or other materials provided with the
##     distribution.
##   * Neither the name of University of Aberdeen nor
##     the names of its contributors may be used to endorse or promote
##     products derived from this software without specific prior written
##     permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
## LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
## DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
## THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
## OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
##
#############################################################################


import sys
import unittest

from PyQt5.QtWidgets import QApplication

from graph.qtgraph import *

G = {'A': ['B', 'C'],
    'B': ['C', 'D'],
    'C': ['D'],
    'D': ['E'],
    'E': ['F'],
    'F': []}


#class TestGraphWidget(unittest.TestCase):
#
#    def setUp(self):
#        self.app = QApplication(sys.argv)
#
#    def tearDown(self):
#        pass
#
#    def test_basics(self):
#        g = Graph(G)
#        graph = GraphWidget(g, "GraphWidget Test")
#        self.assertIsNotNone(graph)
#        graph.forceLayout(True)
#        graph.show()
#        self.app.exec_()
#
#    def test_pdf(self):
#        g = Graph(G)
#        graph = GraphWidget(g, "GraphWidget Test")
#        self.assertIsNotNone(graph)
#        graph.autoLayout()
#        graph.show()
#        graph.savePDF()
#        self.app.exec_()
#
#
#class TestDecorators(unittest.TestCase):
#
#    def setUp(self):
#        self.app = QApplication(sys.argv)
#
#    def test_decorator(self):
#        widget = GraphWidget(Graph(), "Decorator Test")
#        d = Decorator(40, 70, 5, 'blue')
#        d.setPos(350, 330)
#        widget.scene().addItem(d)
#
#        d = DiamondDecorator(50, 100, 5, 'red', 2)
#        d.setPos(450, 250)
#        widget.scene().addItem(d)
#
#        d = LeftTriangleDecorator(50, 100, 5, 'red', 2)
#        d.setPos(50, 200)
#        widget.scene().addItem(d)
#
#        d = RightTriangleDecorator(50, 100, 5, 'red', 2)
#        d.setPos(150, 200)
#        widget.scene().addItem(d)
#
#        d = UpTriangleDecorator(100, 50, 5, 'red', 2)
#        d.setPos(250, 250)
#        widget.scene().addItem(d)
#
#        d = DownTriangleDecorator(100, 50, 5, 'red', 2)
#        d.setPos(250, 200)
#        widget.scene().addItem(d)
#
#        widget.show()
#        self.app.exec_()


class TestNodes(unittest.TestCase):

    def setUp(self):
        self.app = QApplication(sys.argv)

    def test_decorated_node1(self):
        widget = GraphWidget(Graph(G), "Decorated Node Test 1")
        n = widget.getNode('A')
        n.setColour('orange')
        RightTriangleDecorator(n, 'right')
        n.rounded = False

        n = widget.getNode('B')
        DiamondDecorator(n, 'right')
        LeftTriangleDecorator(n, 'left')
        n.rounded = False

        n = widget.getNode('C')
        DownTriangleDecorator(n, 'bottom')
        DiamondDecorator(n, 'left')
        n.rounded = False

        n = widget.getNode('D')
        UpTriangleDecorator(n, 'top')
        n.rounded = False

        n = widget.getNode('F')
        UpTriangleDecorator(n, 'bottom')
        DownTriangleDecorator(n, 'top')
        RightTriangleDecorator(n, 'left')
        LeftTriangleDecorator(n, 'right')
        n.rounded = False

        widget.show()
#        widget.autoLayout()
        self.app.exec_()

    def test_decorated_node2(self):
        widget = GraphWidget(Graph(G), "Decorated Node Test 2")
        n = widget.getNode('F')
        n.setColour('orange')
        n.addDecorator('right_triangle', 'left')

        n = widget.getNode('B')
        n.addDecorator('left_triangle', 'right')
        n.addDecorator('diamond', 'right')

        n = widget.getNode('C')
        n.addDecorator('down_triangle', 'bottom')
        n.addDecorator('diamond', 'right')
        n.removeDecorator('bottom')

        n = widget.getNode('D')
        n.addDecorator('diamond', 'top')
        n.addDecorator('diamond', 'bottom')

        n = widget.getNode('E')
        n.addDecorator('diamond', 'left')

        n = widget.getNode('A')

        widget.show()
        widget.autoLayout()
        self.app.exec_()


#class TestFonts(unittest.TestCase):
#
#    def setUp(self):
#        self.app = QApplication(sys.argv)
#
#    def test_normal(self):
#        widget = GraphWidget(Graph(), "Normal Font test")
#        n = widget.createNode('Hello')
#        widget.show()
#        self.app.exec_()
#
#    def test_italic(self):
#        widget = GraphWidget(Graph(), "Italic Font test")
#        n = widget.createNode('Hello')
#        n.setItalic(True)
#        widget.show()
#        self.app.exec_()
#
#    def test_bold(self):
#        widget = GraphWidget(Graph(), "Bold Font test")
#        n = widget.createNode('Hello')
#        n.setBold(True)
#        widget.show()
#        self.app.exec_()
#
#    def test_size(self):
#        widget = GraphWidget(Graph(), "Font Size test")
#        n = widget.createNode('Hello')
#        n.setFontSize(33)
#        widget.show()
#        self.app.exec_()
#        
#
#class TestShapeTester(unittest.TestCase):
#
#    def setUp(self):
#        self.app = QApplication(sys.argv)
#
#    def test_shape(self):
#        widget = GraphWidget(Graph(), "Shape test")
#        shape = ShapeTester(parent=None)
#        widget.scene().addItem(shape)
#        widget.show()
#        self.app.exec_()







